#!/usr/bin/env python3
import tkinter as tk
import tkinter.ttk as ttk

class SurveyForm(tk.Toplevel):

    def __init__(self, master):
        super().__init__(master)
        self.withdraw()     # hide until ready to show
        self.title("Survey")
        self.accountInformationtext = ttk.Label(self, text='Account Information:')
        self.accountInformationtext.grid(row=0, column=0, sticky=tk.W, padx='0.75m', pady='0.75m')
        self.usernameLabel = ttk.Label(self, text="Username:")
        self.usernameLabel.grid(row=1, column=0, sticky=tk.W, padx="0.75m", pady="0.75m")
        self.usernameEntry = ttk.Entry(self)
        self.usernameEntry.grid(row=1, column=1, sticky=(tk.W, tk.E), padx="0.75m", pady="0.75m")
        self.passwordLabel = ttk.Label(self, text="Password:")
        self.passwordLabel.grid(row=2, column=0, sticky=tk.W, padx="0.75m", pady="0.75m")
        self.passwordEntry = ttk.Entry(self, show="*")
        self.passwordEntry.grid(row=2, column=1, sticky=(tk.W, tk.E), padx="0.75m", pady="0.75m")
        self.surveytext = ttk.Label(self, text='Survey:')
        self.surveytext.grid(row=3, column=0, sticky=tk.W, padx='0.75m', pady='0.75m')
        self.wheredidyoufindthiswebsiteLabel = ttk.Label(self, text="Where did you find this website?:")
        self.wheredidyoufindthiswebsiteLabel.grid(row=4, column=0, sticky=tk.W, padx="0.75m", pady="0.75m")
        self.survey1Entry = ttk.Entry(self)
        self.survey1Entry.grid(row=4, column=1, sticky=(tk.W, tk.E), padx="0.75m", pady="0.75m")
        self.howoftendoyouvisitthissiteLabel = ttk.Label(self, text="How often do you visit this site?:")
        self.howoftendoyouvisitthissiteLabel.grid(row=5, column=0, sticky=tk.W, padx="0.75m", pady="0.75m")
        self.survey2Entry = ttk.Entry(self)
        self.survey2Entry.grid(row=5, column=1, sticky=(tk.W, tk.E), padx="0.75m", pady="0.75m")
        self.submitButton = ttk.Button(self, text="Submit")
        self.submitButton.grid(row=6, column=0, padx="0.75m", pady="0.75m")
        self.bind("<Escape>", lambda *args: self.destroy())
        self.deiconify()    # show when widgets are created and laid out
        if self.winfo_viewable():
            self.transient(master)
        self.wait_visibility()
        self.grab_set()
        self.wait_window(self)

if __name__ == "__main__":
    application = tk.Tk()
    window = SurveyForm(application)
    application.protocol("WM_DELETE_WINDOW", application.quit)
    application.mainloop()
